package com.example.aiforgames;

import com.example.aiforgames.Vector2D;

public class Vector2D {

	
	public Vector2D(Vector2D va) {
		x = va.getX();
		y = va.getY();
	}

	public Vector2D(){}

	private double x;
	private double y;
	
	
	
	public double getX() {
		return x;
	}

	public static Vector2D switchSign(Vector2D va)
	{
		Vector2D vb = new Vector2D();
		vb.setX(-va.getX());
		vb.setY(-va.getY());
		
		return vb;
	}

	@Override
	public String toString() {
		return "Vector2D [x=" + x + ", y=" + y + "]";
	}



	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		long temp;
		temp = Double.doubleToLongBits(x);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(y);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}



	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Vector2D other = (Vector2D) obj;
		if (Double.doubleToLongBits(x) != Double.doubleToLongBits(other.x))
			return false;
		if (Double.doubleToLongBits(y) != Double.doubleToLongBits(other.y))
			return false;
		return true;
	}



	public void setX(double x) {
		this.x = x;
	}



	public double getY() {
		return y;
	}



	public void setY(double y) {
		this.y = y;
	}



	public Vector2D(double x, double y) {
		super();
		this.x = x;
		this.y = y;
	}

	public Vector2D add(Vector2D va)
	{
		x+= va.getX();
		y+= va.getY();
		return this;
	}
	public static Vector2D add(Vector2D va, Vector2D vb)
	{
		Vector2D vc = new Vector2D();
		vc.add(va).add(vb);
		return vc;
	}
	
	public Vector2D mulConst(double constant)
	{
		x*= constant;
		y*= constant;
		return this;
	}
	public static Vector2D mulConst(Vector2D va,double constant)
	{
		return new Vector2D(va).mulConst(constant);
	}
	
	public Vector2D nomarlize()
	{
		double module = Math.sqrt(x*x+y*y);
		x= x/module;
		y = y / module;
		return this;
	}
	public Vector2D nomarlize(Vector2D va)
	{
		double module = Math.sqrt(va.getX()*va.getX() + va.getY()*va.getY());
		Vector2D vb = new Vector2D();
		vb.setX(va.getX()/module);
		vb.setY(va.getY()/module);
		return vb;
	}
}
