package com.example.aiforgames;

import com.example.aiforgames.Vector2D;

public class SteeringOutput {

	private Vector2D linear;
	private double angular;
	public Vector2D getLinear() {
		return linear;
	}
	public void setLinear(Vector2D linear) {
		this.linear = linear;
	}
	public double getAngular() {
		return angular;
	}
	public void setAngular(double angular) {
		this.angular = angular;
	}
	public SteeringOutput() {
		super();
	}
}
