package com.example.aiforgames;

import com.example.aiforgames.Kinematic;
import com.example.aiforgames.SteeringOutput;
import com.example.aiforgames.Vector2D;

public class Kinematic {

	private Vector2D position;
	private double oreention;
	private Vector2D velocity;
	private double rotation;

	

	public Vector2D getPosition() {
		return position;
	}

	public void setPosition(Vector2D position) {
		this.position = position;
	}

	public double getOreention() {
		return oreention;
	}

	public void setOreention(double oreention) {
		this.oreention = oreention;
	}

	public Vector2D getVelocity() {
		return velocity;
	}

	public void setVelocity(Vector2D velocity) {
		this.velocity = velocity;
	}

	public double getRotation() {
		return rotation;
	}

	public void setRotation(double rotation) {
		this.rotation = rotation;
	}

	
	
	
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		long temp;
		temp = Double.doubleToLongBits(oreention);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + ((position == null) ? 0 : position.hashCode());
		temp = Double.doubleToLongBits(rotation);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + ((velocity == null) ? 0 : velocity.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Kinematic other = (Kinematic) obj;
		if (Double.doubleToLongBits(oreention) != Double.doubleToLongBits(other.oreention))
			return false;
		if (position == null) {
			if (other.position != null)
				return false;
		} else if (!position.equals(other.position))
			return false;
		if (Double.doubleToLongBits(rotation) != Double.doubleToLongBits(other.rotation))
			return false;
		if (velocity == null) {
			if (other.velocity != null)
				return false;
		} else if (!velocity.equals(other.velocity))
			return false;
		return true;
	}

	public void Update(SteeringOutput steering, double time){
	
		double haldTimeSquare = time*time/2;
		position.add(Vector2D.mulConst(velocity, time)).add(Vector2D.mulConst(steering.getLinear(), haldTimeSquare));
		oreention += rotation*time + steering.getAngular() * haldTimeSquare;
		velocity.add(Vector2D.mulConst(steering.getLinear(), time));
		rotation += steering.getAngular() * time;
		
	}

	public static double newOrientation(double current, Vector2D newVelocity){

		if(true)
			return Math.atan2(-newVelocity.getX(), newVelocity.getY());
		return current;
	}
}
