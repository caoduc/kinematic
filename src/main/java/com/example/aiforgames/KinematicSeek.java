package com.example.aiforgames;

import com.example.aiforgames.Kinematic;
import com.example.aiforgames.KinematicSeek;
import com.example.aiforgames.SteeringOutput;
import com.example.aiforgames.Vector2D;

public class KinematicSeek {

private double maxSpeed;
	
	private Kinematic character;
	private Kinematic target;
	
	public double getMaxSpeed() {
		return maxSpeed;
	}

	public void setMaxSpeed(double maxSpeed) {
		this.maxSpeed = maxSpeed;
	}

	public Kinematic getCharacter() {
		return character;
	}

	public void setCharacter(Kinematic character) {
		this.character = character;
	}

	public Kinematic getTarget() {
		return target;
	}

	public void setTarget(Kinematic target) {
		this.target = target;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((character == null) ? 0 : character.hashCode());
		long temp;
		temp = Double.doubleToLongBits(maxSpeed);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + ((target == null) ? 0 : target.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		KinematicSeek other = (KinematicSeek) obj;
		if (character == null) {
			if (other.character != null)
				return false;
		} else if (!character.equals(other.character))
			return false;
		if (Double.doubleToLongBits(maxSpeed) != Double.doubleToLongBits(other.maxSpeed))
			return false;
		if (target == null) {
			if (other.target != null)
				return false;
		} else if (!target.equals(other.target))
			return false;
		return true;
	}
	public SteeringOutput getSteering() {
		SteeringOutput ste = new SteeringOutput();
		ste.setLinear(Vector2D.add(target.getPosition(), Vector2D.switchSign(character.getPosition())));
		
		ste.getLinear().nomarlize().mulConst(maxSpeed);
		character.setOreention(Kinematic.newOrientation(character.getOreention(),ste.getLinear()));
		character.setRotation(0);
		return ste;
	}
}
